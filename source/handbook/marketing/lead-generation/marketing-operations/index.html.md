---
layout: markdown_page
title: "Marketing Operations"
---
Welcome to the Marketing Operations Handbook.

[Up one level to the Demand Generation Handbook](https://about.gitlab.com/handbook/marketing/lead-generation/)    

----

## On this page
* [Overview](#overview)
* [General Lead Gen Information](#leadgeninfo)

----

## Overview <a name="overview"></a>
- [Scope of Responsibility](#sor)
- [Marketing Tech Stack](#mktgtechstack)


## General Lead Gen Information <a name="leadgeninfo"></a>
- [Leads](#leads)
    - [Lead Routing](#leadroute)
    - [MQL Definition](#mql)
        - - [Lead Scoring Model](MQLmodel)
    - [Inbound Lead Flow](#leadFlow)
    - [Lead Qualification Process](#leadQual)
    - [What counts as an MQL, SQL, or SAL?](#SQL)
    - [SQL Qualification Criteria](#sqlQual)
    - [Lead Status](#leadStatus)
    - [Passing Qualified Leads](#leadPassing)
    - [Nurture campaign process](#nurture)
    - [Subscriptions and Newsletter](#newsletter)
    - [Inbound Leads](#inbound)
    - [New license flow](#licenseFlow)
    - [Marketo Tools Server](#marketoTools)
    - [Sales and Community Group Emails](#groupEmail)
- [Marketo Training Videos](#mktotraining)

## Overview

### Scope of Responsibility <a name=“sor”></a>

Marketing Operations (Mktg OPS) supports the entire Marketing team as well as other teams within GitLab. Mktg OPS works closely with Sales Operations to ensure information between systems is seamless and we are using consistent terminology in respective systems. 	

### Marketing Tech Stack <a name=“mktgtechstack”></a>

Our current marketing tech stack consists of the following tools: 
- Clearbit: Enrichment tool to add details to customer & prospect record to aide in sales process and overall database segmentation.
- DiscoverOrg: Industry specific data enrichment for leads/contacts/accounts
- FunnelCake: Buyer lifecycle and funnel analytics tool
- Google Analytics: Web analytics tool that tracks and reports website traffic
- Google Tag Manager: Tool to add/update website tags, incl. conversion tracking, site analytics, remarketing, without needing to edit website code
- InsideView: Lead enrichment tool
- Marketo: Marketing automation and database management tool
- On24: Webcast software providing live and on-demand recordings
- Outreach.io: Email communication tool for the BDR/SDR/AEs to manage communication with leads/contacts
- Piwik: Open source web analytics platform used to measure web performance
- Salesforce: Customer Relationship Management (CRM) tool used to manage all customer & prospect data. 
- Unbounce: Custom landing page tool specific for driving conversions 

[Tech stack](https://about.gitlab.com/handbook/marketing/lead-generation/marketing-operations/marketing_tech_stack/) contract details, admins and other miscellaneous information. 

## Leads <a name=“leads”></a>

### Lead Routing <a name="leadroute"></a>

This information will be updated shortly and live in the [BDR section](https://about.gitlab.com/handbook/marketing/business-development/) of the handbook so it is not duplicated. 

### MQL Definition <a name="mql"></a>

A Marketing Qualified Lead (MQL) is a lead that reached a certain point threshold based on collection of demographic information, engagement and/or behavioural data. We have two MQL "buckets": (1) hand raisers and (2) passive leads.

#### Hand Raisers (A Leads)

Hand raisers are leads who have filled out a Sales Contact Us form or signed up for a free Enterprise Edition (EE) Trial. These leads are automatic MQLs regardless of demographic information because they have exhibited clear interest in GitLab's ability to fulfill a software need.

#### Passive Leads

Passive leads are leads who have not completed an A Lead action (filling out contact form or taking out a EE Trial). For a passive lead to become an MQL, they must meet our minumum lead score of 90 points. A passive lead becomes an MQL via a combination of demographic score and behaviour/engagement score. See [lead scoring model](#MQLmodel) below. 

#### Qualifying by Demographics 

We have determined values for specific demographic information (i.e. job title, company name, industry, valid work email, company size) based on our target buyer. Currently, our marketing inbound lead generation funnel is focused on serving up Small Business (SMB) decision makers to our Business Development Representatives (BDRs). Assigning positive points to specific demographic information (e.g. job title is "Head, Director, Senior, etc.") allows us to surface leads who are more likely to be a decision maker, and is built to make it more difficult for individual developer/engineer roles to MQL.

#### Qualifying by Behaviour and Engagement

Behaviour is tracked by how and where our leads are engaging with us. This system is intentionally set up to surface people showing stronger buying intent signs than others.  

We recognize that not all people who engage with our webcasts, content campaigns or open our emails should become MQLs, so the system is set up so somene has to engage multiple times in order to MQL from these activities. Visiting specific pages (i.e. product, features, comparison pages, visited page but didn't complete EE Trial form) shows a higher level of intent thus scored higher.

### Lead Scoring Model<a name="MQLmodel"></a>

This [model](https://docs.google.com/a/gitlab.com/document/d/1_kSxYGlIZNNyROLda7hieKsrbVtahd-RP6lU6y9gAIk/edit?usp=sharing) is based on a 100-point system. Positive and negative points are assigned to a lead based on their demographic and firmographic information, and their behaviour and/or engagement with the GitLab website and marketing campaigns.  
This model was implemented on 13 February 2017 and only impacted leads moving forward. For reporting purposes, it was decided not to retroactively adjust scores and MQL dates. 

### Inbound Lead Flow<a name="leadFlow"></a>

1. Lead comes into BDR team via one of the named Inbound Lead routes above.
2. Lead is input into Marketo.
3. Lead is assigned according to assignment rules.
4. If region is EMEA, lead goes directly to EMEA BDR team.
5. If region is APAC, lead goes directly to APAC Sales Director.
6. All other regions go directly to NA BDR team.
7. All other leads pass through BDR lead qualification process.

### Lead qualification process<a name="leadQual"></a>

1. Unless a specific request is made, provide a useful resource that will help the person have a better GitLab experience.
2. Ask [Discovery Questions](https://about.gitlab.com/handbook/sales-qualification-questions/) to qualify lead
3. The following [criteria](#sqlQual) is used to determine if a lead should be passed to sales or recommended CE resources. Once determined, BDR team passes all leads to sales for followup via Salesforce assignment and email notification.
4. If further qualification is needed to understand SQL Qualification requirements, BDR team will email or schedule a phone call with lead to understand their project and initiatives.
5. Once a lead has met the criteria for an SQL, the BDR will schedule a discovery call with the prospect and an AE.  On the call, the BDR will provide a warm introduction and handoff the prospect to the AE.
6. If SQL criteria isn't met and there are questions, BDR team will answer all questions or route to support.
7. If there are no questions and lead isn't qualified yet, the lead status is updated appropriately. See "lead status" above.
8. All Account Executives and Senior Account Executives are part of the regular Lead round robin rotation but if a lead is from a [Fortune 500 company](http://fortune.com/fortune500/), it will be assigned to a Senior Account Executive.  For larger opportunities outside the US, lead will be passed to senior account executive or sales director in region.
9. If a lead is an existing customer or a prospect that's owned/operated by an existing customer _but is not using EE_, BDR team will determine account owner and pass lead.
10. If a lead is from an existing account _and is using EE_, the BDR will convert the lead to a contact in SFDC (making sure to check the “Do not create a new opportunity” box) and @mention the lead owner in SFDC to let them know of the new contact. No need to connect the lead with the owner via email.
11. If a lead is from a company that is already in Salesforce, BDR team will determine account owner and pass lead.

### What counts as an MQL, SQL, or SAL?<a name="SQL"></a>
* SQL is any lead which meets the [SQL Qualification Criteria](#sqlQual)

        => when converting a qualified lead, make sure that you are creating an opportunity upon conversion. This is required to map the BDR to the opportunity.  It also helps us to track the age of an opportunity from discovery call to closed.

        => if the lead is not a new opportunity, convert to a contact and uncheck the box to create a new opportunity.
* SAL is an SQL which has been accepted by the sales team
* MQL is any lead with a lead score of 20 or greater in Marketo (lead score is calculated based on behavioral and demographic data). Some examples of bahavior or demographic data that increase lead score are:
   * Signing up to receive the newsletter (behavioral)
   * Signing up for an account on gitlab.com (behavioral)
   * Filling out other web forms (contacts us, webinar registration, etc) (behavioral)
   * Starting an EE Trial (behavioral)
   * Working at a Fortune 500 (demographic)
   * Job title (demographic)

### SQL Qualification Criteria <a name="sqlQual"></a>

1. **Current Defined Need:** Does the prospect have an identified need for GitLab?  List out the current need.
2. **Current Defined Need:** What is the prospect currently doing to address their need?  What other technologies are they using?
3. **Budget:** Does the prospect have a realistic chance of securing the budget for GitLab?  Is there already a budget secured for the project?
4. **Buying Process:** Who is the decision maker for GitLab?
5. **Buying Process:** What is the buying process for procuring GitLab?
6. **Buying Process:** What is role does the prospect play in the current evaluation (function, job title)?
7. **Timeline:** What is the timeline to make a decision?  Are you currently in an existing contract that needs to expire before you can move forward? If yes, when does the contract expire?
8. **Product Fit:** Are they interested in GitLab EE?  Is GitLab EE a good fit for their need? Needs to be YES
9. **Product Fit:** Do they currently use another version of GitLab?  Are they familiar with our product family?
10. **Scope:** How many seats are they interested in purchasing? This will be needed to add in the opportunity amount field "example 100 x $39 = $3900"
11. **Scope:** How many developers total (potential additional seats) do they have?  Anything over 100 potential seats is assigned to an AE.  Leads under 100 potential seats will be assigned to BDR Manager to track.
12. **Next Steps:** Is there a meeting set with an AE to discuss next steps? Meeting needs to be set before lead can be qualfiied.
13. **Existing Client** Is the contact from a new business unit? If existing business unit refer to AE responsible for the account.


### Lead status<a name="leadStatus"></a>

- Raw => Newly synced lead from Marketo to SFDC
- Open => Lead not yet contacted
- Attempt 1 => One message sent without response; after 21 days, auto-updates to Nurture
- Attempt 2 => Two messages sent without response; after 21 days, auto-updates to Nurture
- Attempt 3 => Three messages sent without response; after 21 days, auto-updates to Nurture
- Attempt 4 => Four messages sent without response; after 21 days, auto-updates to Nurture
- Qualified => Soft-BANT criteria met. Action: pass to sales team.
- Progressing => Communication is two-way, but Soft-BANT still undetermined. Any response that doesn't immediately put the lead into "Qualified", "Unqualified", or "Nurture" status should put the lead in this status.
- Unqualified => Soft-BANT criteria not met (e.g. a developer using GitLab for personal projects, a student, etc.). Action: send appropriate resources if requested; avoid sending salesy messages.
- Bad Data => Invalid email address. Note: only mark as bad data if there is no communication channel. For example, a lead who provided "Spam Spamson" as their name but still provided a valid email address is not Bad Data.
- Nurture => May become "Qualified" in the future, but communication is closed. Will receive useful marketing emails.

### Passing Qualified Leads<a name="leadPassing"></a>

1. BDR checks AE availability and send invite to prospect(s) and AE with invite naming convention of Meeting Invite Naming Convention: Gitlab Discovery Call - with call in details and agenda of meeting (from summary email and notes)
1. BDR emails prospect, cc'ing AE. Email consists of a summary of the lead qual data captured above (current state, problems to solve, what they would like to learn and desired state).  This email also introduces the AE and confirms the meeting day with the prospect and informs them that a meeting invite will be sent shortly.
1. BDR converts lead to opportunity.  BDR adds into revenue into the amount field, within the opportunity object, based on the lead criteria uncovered of number of seats and/or products interested in purchasing now.
1. If technical resources will be needed for any calls, AE will be responsible for inviting necessary resources.  Typically, the frst call, which isi Discovery, should not contain technical discussion or resources as we are gathering information from prospect to present information at a later date.
1. BDR joins Discovery call and provides warm handoff

### Nurture campaign process<a name="nurture"></a>

Coming soon once process is defined. Will be signup campaign for GitLab.com, leads that don't meet Soft-BANT requirements, etc.

### Subscriptions and Newsletter<a name="newsletter"></a>

Inbound leads receive appropriate marketing emails, such as newsletters, onboarding tips (coming soon), etc. What they receive depends on how they came to find us and what we believe will be most helpful to them. For example, a person who downloaded an EE trial will receive different resources than a person who registered for a webcast. Our non-operational emails have a one-click unsubscribe button. You can manually unsubscribe a person by clicking the "Opt Out" checkbox in SFDC. SFDC also has a manual 30-Day Opt Out checkbox for a 30-day unsubscribe from non-operational emails.

### Inbound Leads<a name="inbound"></a>

- https://about.gitlab.com/pricing/ => Currently when someone clicks "Buy Now" it creates an account in Zuora and Salesforce. The account owner is notified, if account does not exist the account owner is sales admin.
- https://about.gitlab.com/free-trial/ => Free trial submits flow to BDR team. lead submits form, form sends data to Marketo, Marketo requests license key from the licensing app, lead gets email with license key from Marketo. BDR follows lead qualification process (documented below).
- https://about.gitlab.com/sales/ => When a lead submit a form to contact sales, that lead flows through marketo and notifies BDR team. BDR follows lead qualification process (documented below).
- https://about.gitlab.com/contact/ Email to community@gitlab.com - Email sends to marketing team. Leads for EMEA are handled by EMEA BDR, Leads for NA are handled by NA BDR, APAC leads are first notice, first route. When lead is followed up, please BCC community@gitlab.com so everyone knows it has been handled and we don't duplicate work. BDR follows lead qualification process (documented below).
- https://about.gitlab.com/contact/ Newsletter signup - lead is added to our semi-monthly newsletter.
- https://about.gitlab.com/contact/ Security signup - Signs up lead for security notices.
- Emails to sales@gitlab.com => Email to sales@gitlab.com - Email sends to sales team. Leads for EMEA are handled by EMEA BDR, Leads for NA are handled by NA BDR, APAC leads are first notice, first route. When lead is followed up, please BCC community@gitlab.com so everyone knows it has been handled and we don't duplicate work. BDR follows lead qualification process (documented below).
- https://about.gitlab.com/press/ press@gitlab.com => Forwards to CMO for handling with our PR team.
- https://about.gitlab.com/development/ => When a lead submit a form to request development help, that lead flows through Marketo and notifies BDR team. BDR follows lead qualification process (documented below).
- https://about.gitlab.com/training/ => When a lead submit a form for training, that lead flows through Marketo and notifies BDR team. BDR follows lead qualification process (documented below).
- https://about.gitlab.com/consultancy/ => When a  submit a form for consultancy, that lead flows through Marketo and notifies BDR team. BDR follows lead qualification process (documented below).
- GitLab.com lead signup =>
- https://about.gitlab.com/downloads/ GitLab CE Downloads newsletter signups=> Lead flows through BDR team for lead qualification process (documentation below).
- Webcast leads => Leads registered for a webcast are sent confirmation, reminder, and followup (recording link and attendee survey) emails. A new lead flows through BDR team for lead qualification process.
- Any other forms that are created simply flow through the BDR team for lead qualification process (documentation below).

### New license flow<a name="licenseFlow"></a>

Current state
1. "Buy Now" button on https://about.gitlab.com/pricing/ is submitted.
2. Zuora intakes order and sends account information to Salesforce.
3. Account owner is notifed.
4. If no current account owner, sales admin is the account owner.

### Marketo Tools Server<a name="marketoTools"></a>

- This is a simple Sinatra application that receives several webhooks and forwards the relevant information to Marketo via its REST API.
- URL: http://marketo-tools.gitlap.com/ (can't access via browser)
- [Software running on it](https://gitlab.com/gitlab-com/marketo-tools/)
- [Readme of the cookbook for deployment](https://gitlab.com/gitlab-com/cookbook-marketo-tools/blob/master/README.md)

### Sales and Community Group Emails<a name="groupEmail"></a>

- Always CC or BCC the group email list (sales@gitlab.com or community@gitlab.com) so others on the list know it's been handled.
- If a request is a new lead with questions about EE, pricing, etc., handle directly.
- If an email is a quote, forward to appropriate sales people.
- If an email is a refund or other billing request, forward to ar@gitlab.com.
- If an email is a license issue or question, forward to support@gitlab.com.
- If an email is received (contact request, account questions, etc. etc.), check in sfdc if there is an account owner. If there is, forward the email to the owner and change the case into their name if there is an open case. If the account owner is Chad or Hank, this is the default. Forward all inquiries/requests to Chad and he will take care of them. Also switch the sfdc case into Chad's name as well.

## Marketo Training Videos<a name="mktotraining"></a>

Here are links to the recordings of Marketo trainings that we've done:

* [Email Creation and Tokens](https://drive.google.com/open?id=0B1_ZzeTfG3XYZjUwa3ZFb2ZWeWc)
* [How to Use Smart Lists and Reporting](https://drive.google.com/open?id=0B1_ZzeTfG3XYcGV2VUFiR0dNaWM)

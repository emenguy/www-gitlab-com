---
layout: markdown_page
title: "Edge Team"
---

## On this page
{:.no_toc}

- TOC
{:toc}

----

#### Edge Team Overview

The Edge Team is part of engineering responsible for improving GitLab
in multiple ways:

* Improving GitLab code/workflows in ways that engineering groups may not be doing:
  * Increasing contributors productivity by improving the development setup,
    workflow, processes, and tools
  * Reducing the duration of the GitLab test suite, and improving its stability
    by eliminating transient failures
  * Identifying architectural and [backstage](/jobs/specialist/backstage/)
    improvements as well as technical debt
* Interfacing with the community
  * [Triaging issues](/jobs/specialist/issue-triage/) reported by the community
  * [Reviewing and accepting merge requests](/jobs/merge-request-coach/) submitted by the community
* Advancing GitLab's contributions to other related open source projects
  * For example: Git command-line
* Evaluating code quality in potential partnerships

#### Edge Team Resources

* [Issue Triage](/handbook/edge/issue-triage/)
